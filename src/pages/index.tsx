import Head from "next/head";
import Image from "next/image";
import { Inter } from "next/font/google";

const inter = Inter({ subsets: ["latin"] });

export default function Home() {
    return (
        <>
            <Head>
                <title>BIT3134 Final Project API</title>
                <meta
                    name="description"
                    content="API for excel project for BIT class"
                />
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1"
                />
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <main>
                <h2>BIT3134 Final Project API, Not for Public USE</h2>
            </main>
        </>
    );
}
