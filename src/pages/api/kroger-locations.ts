// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import {
    getAccessToken,
    getLocations,
    searchForProducts,
} from "@/utils/kroger";
import { processLocationsForExcel } from "@/utils/processForExcel";
import axios from "axios";
import type { NextApiRequest, NextApiResponse } from "next";

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse
) {
    const {
        query: { zipCode },
    } = req;

    if (!zipCode || Array.isArray(zipCode)) {
        return res.status(400).end();
    }

    const zipCodeNum = parseInt(zipCode);

    if (isNaN(zipCodeNum)) {
        return res.status(400).end();
    }

    const accessToken = await getAccessToken();

    const locations = await getLocations(zipCodeNum, accessToken);

    return res.status(200).json(processLocationsForExcel(locations));
}
