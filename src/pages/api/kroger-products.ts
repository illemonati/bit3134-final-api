// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import { getAccessToken, searchForProducts } from "@/utils/kroger";
import { processProductsForExcel } from "@/utils/processForExcel";
import axios from "axios";
import type { NextApiRequest, NextApiResponse } from "next";

type Data = {
    name: string;
};

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse
) {
    const {
        query: { itemSearchName, locationId },
    } = req;

    if (
        !itemSearchName ||
        Array.isArray(itemSearchName) ||
        Array.isArray(locationId)
    ) {
        return res.status(400).end();
    }

    const accessToken = await getAccessToken();

    const products = await searchForProducts(
        itemSearchName,
        accessToken,
        locationId
    );

    return res.status(200).json(processProductsForExcel(products));
}
