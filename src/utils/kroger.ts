import axios from "axios";
import { LocationData, ProductData } from "./kroger-types";

let accessToken: string | null = null;

export const getAccessToken = async (forceNew?: boolean) => {
    const clientId = process.env.KROGER_CLIENT_ID;
    const clientSecret = process.env.KROGER_CLIENT_SECRET;

    if (accessToken && !forceNew) return accessToken;

    const token = Buffer.from(`${clientId}:${clientSecret}`).toString("base64");

    const auth = `Basic ${token}`;

    const resp = await axios.post(
        "https://api.kroger.com/v1/connect/oauth2/token",
        {},
        {
            headers: {
                Authorization: auth,
                "Content-Type": "application/x-www-form-urlencoded",
            },
            params: {
                grant_type: "client_credentials",
                scope: "product.compact",
            },
        }
    );

    const { access_token } = resp.data;
    accessToken = access_token;
    return access_token;
};

export const searchForProducts = async (
    name: string,
    accessToken: string,
    locationID?: string,
    retry?: boolean
): Promise<ProductData> => {
    let params: {
        "filter.term": string;
        "filter.locationId"?: string;
    } = {
        "filter.term": name,
    };

    if (locationID) {
        params = {
            ...params,
            "filter.locationId": locationID,
        };
    }

    try {
        const resp = await axios.get("https://api.kroger.com/v1/products", {
            headers: {
                Authorization: `Bearer ${accessToken}`,
                "Content-Type": "application/json",
            },
            params,
        });
        return resp.data;
    } catch (e) {
        if (!retry) {
            const accessToken = await getAccessToken(true);
            return await searchForProducts(name, accessToken, locationID, true);
        } else {
            throw e;
        }
    }
};

export const getLocations = async (
    zip: number,
    accessToken: string,
    retry?: boolean
): Promise<LocationData> => {
    try {
        const resp = await axios.get("https://api.kroger.com/v1/locations", {
            headers: {
                Authorization: `Bearer ${accessToken}`,
                "Content-Type": "application/json",
            },
            params: {
                "filter.zipCode.near": zip,
            },
        });
        return resp.data;
    } catch (e) {
        if (!retry) {
            const accessToken = await getAccessToken(true);
            return await getLocations(zip, accessToken, true);
        } else {
            throw e;
        }
    }
};
