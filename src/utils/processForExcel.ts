import { LocationData, ProductData } from "./kroger-types";

export const processLocationsForExcel = (locations: LocationData) => {
    return locations.data.map((location) => ({
        id: location.locationId,
        name: location.name,
        address: location.address.addressLine1,
    }));
};

export const processProductsForExcel = (products: ProductData) => {
    return products.data
        .map((product) => {
            if (product.items.length < 1) {
                return null;
            }
            const price =
                product.items[0].price.promo > 0
                    ? product.items[0].price.promo
                    : product.items[0].price.regular;
            const size = product.items[0].size;

            const units = parseFloat(size.replace(/\D/g, ""));
            const pricePerUnit = price / units;

            return {
                id: product.items[0].itemId,
                description: product.description,
                size,
                price,
                pricePerUnit,
            };
        })
        .filter((a) => a != null);
};
